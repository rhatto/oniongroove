# Oniongroove Threat Model

## Threat Model

Initial considerations:

* To not be repetitive or duplicate efforts, Oniongroove threat model begins by
  inheriting the following more general analysis:
    * [Tor: The Second-Generation Onion Router - Threat Model](https://svn-archive.torproject.org/svn/projects/design-paper/tor-design.html#subsec:threat-model).

    * [The Design and Implementation of the Tor Browser - Adversary Model](https://2019.www.torproject.org/projects/torbrowser/design/#adversary)

    * [GitHub - Attacks-on-Tor/Attacks-on-Tor: Thirteen Years of Tor Attacks](https://github.com/Attacks-on-Tor/Attacks-on-Tor#threat-model)

* Some setups might have security requirements such as hardened hosting
  on premises/baremetal (i.e, outside the cloud or third-party providers).

  If backend keys are disposable, then the operators could consider whether
  only the frontend instance(s) should be in an evironment of maximum control
  (such as a hardened/secured baremetal on their datacenter). Backend keys
  could then be periodically rotated (as a mitigation for the current lack
  of implementation for offline .onion keys) ou replaced in case of detected
  compromise, reducing eventual damages to shorter periods (if [Onionbalance][]
  support such rotations).

[Onionbalance]: https://onionservices.torproject.org/apps/base/onionbalance/
