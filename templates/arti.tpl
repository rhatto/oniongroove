#
# Oniongroove configuration for Arti.
#

# By default, Arti program is set to run as a proxy.
# But ideally, for Onion Services it would be better if we could just
# disable SOCKS listening altogether.
#
# Related issue:
# https://gitlab.torproject.org/tpo/core/arti/-/issues/1569
[proxy]
socks_listen = 9050

# Vanguards configuration
[vanguards]
mode = "full"

{% for name, endpoint in endpoints|items %}
[onion_services."{{ name }}"]

# A description of what to do with incoming connections to different ports.
# This is given as a list of rules; the first matching rule applies.

proxy_ports = [
    # Forward HTTP and HTTPs ports on the service to the rewriting proxy
    ["80", "127.0.0.1:80"],
    ["443", "127.0.0.1:443"],
]
{% endfor %}
