#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Oniongroove.
#
# Copyright (C) 2024 The Tor Project, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import json
import subprocess

from .config import basepath

class OniongrooveOpenResty:
    """
    Oniongroove class with OpenResty logic.
    """

    def openresty_config(self):
        """
        Configure Arti.

        :rtype:  bol
        :return: True on success, false if some problem happens.
        """

        mappings = { }

        for name, endpoint in self.config['endpoints'].items():
            if endpoint['type'] != 'proxy':
                continue

            # Get the Onion Services' addresses through Arti
            #
            # Does this by basically invoking arti:
            # docker-compose exec arti arti hss -c /srv/arti/configs/oniongroove.toml --nickname name onion-name
            try:
                args = ['docker-compose', 'exec', 'arti', 'arti',
                        '--log-level', 'error', 'hss', '-c',
                        '/srv/arti/configs/oniongroove.toml', '--nickname',
                        name, 'onion-name' ]

                process = subprocess.Popen(
                        args=args,
                        cwd=basepath,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT,
                        )

                for line in iter(process.stdout.readline, b''):
                    addr = line.strip().decode('utf-8')

                    # Check if it's a valid Onion Service address.
                    # Do not use python-stem, since we don't want to have it as a dependency.
                    if self.is_valid_hidden_service_address(
                            self.get_pubkey_from_address(addr), 3) is False:
                        self.log('Invalid onion service address set for {}: {}'.format(
                            endpoint, addr, 'error'))

                        return False

                process.stdout.close()

            except Exception as e:
                self.log(e, 'error')

            # Add the mapping
            mappings[endpoint['upstream']] = addr

            # Log the mapping
            self.log('Mapping ' + name + ' service to ' + addr + ' with endpoint ' + endpoint['upstream'])

        # Log the mapping
        #self.log('Mappings: ' + str(mappings))

        # Determine the OpenResty config
        openresty_config = os.path.join(basepath, 'configs', 'openresty', 'onionproxy.json')

        # Create a JSON config
        output = json.dumps(mappings, sort_keys=True, indent=4)

        # Write OpenResty config file
        with open(openresty_config, 'w', encoding='utf-8') as f:
            f.write(output)
        return True
