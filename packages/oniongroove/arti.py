#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Oniongroove.
#
# Copyright (C) 2024 The Tor Project, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os

from .config import basepath

class OniongrooveArti:
    """
    Oniongroove class with Arti integration logic.
    """

    def arti_config(self):
        """
        Configure Arti.

        :rtype:  bol
        :return: True on success, false if some problem happens.
        """

        from jinja2 import Environment, FileSystemLoader, select_autoescape

        self.log('Compiling Arti config...')

        # Configure the template loader
        env = Environment(
                loader=FileSystemLoader(os.path.dirname(self.arti_template)),
                autoescape=select_autoescape()
                )

        # Render the template
        arti_config = os.path.join(basepath, 'configs', 'arti', 'oniongroove.toml')
        template    = env.get_template(os.path.basename(self.arti_template))
        output      = template.render(
                endpoints=self.config['endpoints'],
                )

        # Write Arti config file
        with open(arti_config, 'w', encoding='utf-8') as f:
            f.write(output)

        return True
