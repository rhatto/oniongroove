#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Oniongroove.
#
# Copyright (C) 2024 The Tor Project, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import time
import json
import subprocess

from .config import basepath

class OniongrooveMain:
    """
    Oniongroove class with main application logic.
    """

    def run(self):
        """
        Main application loop

        :rtype:  bol
        :return: True on success, false if some problem happens.
        """

        # Check for endpoints
        if 'endpoints' not in self.config:
            self.log('No endpoints defined in config', 'error')

            return False

        if self.args.action == 'start':
            # Start Oniongroove
            self.start()

        elif self.args.action == 'stop':
            # Stop Oniongroove
            self.stop()

        elif self.args.action == 'restart':
            # Restart
            self.restart()

        elif self.args.action == 'status':
            # status
            self.status()

        else:
            self.log('Invalid action "' + self.args.action + '"')

            return False

        return True

    def start(self):
        """
        Starts Oniongroove

        :rtype:  bol
        :return: True on success, false if some problem happens.
        """

        self.log('Starting Oniongroove...')

        try:
            # Build Arti config
            if not self.arti_config():
                return False

            args    = ['docker-compose', 'up', '-d']
            process = subprocess.Popen(
                    args=args,
                    cwd=basepath,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    )

            for line in iter(process.stdout.readline, b''):
                self.log('Docker: ' + line.strip().decode('utf-8'))

            process.stdout.close()

        except Exception as e:
            self.log(e, 'error')

        # Rehash the configuration
        self.rehash()

        return True

    def rehash(self):
        """
        Rebuild Oniongroove configuration.

        Arti must run before the OpenResty config is created, so we
        can get all Onion Service addresses to build the rewriting
        proxy mappings.

        :rtype:  bol
        :return: True on success, false if some problem happens.
        """

        # Wait a bit for Arti to generate any missing Onion Service keys
        sleep = 5
        self.log('Sleeping ' + str(sleep) + ' seconds...')
        time.sleep(sleep)

        # Build OpenResty config
        if not self.openresty_config():
            return False

        return True

    def stop(self):
        """
        Stop Oniongroove.

        :rtype:  bol
        :return: True on success, false if some problem happens.
        """

        self.log('Stopping Oniongroove...')

        try:
            args    = ['docker-compose', 'down']
            process = subprocess.Popen(
                    args=args,
                    cwd=basepath,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    )

            for line in iter(process.stdout.readline, b''):
                self.log('Docker: ' + line.strip().decode('utf-8'))

            process.stdout.close()

        except Exception as e:
            self.log(e, 'error')

        return True

    def restart(self):
        """
        Restart Oniongroove.

        :rtype:  bol
        :return: True on success, false if some problem happens.
        """

        # Stop, then start
        self.stop()
        self.start()

        return True

    def status(self):
        """
        Oniongroove status.

        :rtype:  bol
        :return: True on success, false if some problem happens.
        """

        self.log('Oniongroove status:')

        try:
            args    = ['docker-compose', 'ps']
            process = subprocess.Popen(
                    args=args,
                    cwd=basepath,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    )

            for line in iter(process.stdout.readline, b''):
                self.log('Docker: ' + line.strip().decode('utf-8'))

            process.stdout.close()

        except Exception as e:
            self.log(e, 'error')

        return True
