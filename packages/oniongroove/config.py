#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Handle Oniongroove configurations.
#
# Copyright (C) 2024 The Tor Project, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import argparse

# The Oniongroove version string
# Uses Semantic Versioning 2.0.0
# See https://semver.org
oniongroove_version = '0.0.1'

# The base path for this project
basepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir) + os.sep

# The Arti config template
arti_template = os.path.join(basepath, 'templates', 'arti.tpl')

# Describe configuration options
config = {
        'log_level': {
            'help'    : 'Log level : debug, info, warning, error or critical',
            'default' : 'info',
            'action'  : 'store',
            },
        }

def cmdline_parser():
    """
    Generate command line arguments

    :rtype: argparse.ArgumentParser
    :return: The parser object
    """

    epilog = """Examples:

      oniongroove start configs/oniongroove.yaml
    """

    description = 'Onion Services manager'
    parser      = argparse.ArgumentParser(
                    prog='oniongroove',
                    description=description,
                    epilog=epilog,
                    formatter_class=argparse.RawDescriptionHelpFormatter,
                  )

    parser.add_argument('action', help="""
                        Action to take (start, stop, restart).""".strip())

    parser.add_argument('config', help="""
                        Read options from configuration file. All command line
                        parameters can be specified inside a YAML file.
                        Additional command line parameters override those set
                        in the configuration file.""".strip())

    parser.add_argument('-v', '--version', action='version', version='%(prog)s ' + oniongroove_version)

    for argument in sorted(config):
        config[argument]['type'] = type(config[argument]['default'])

        if not isinstance(config[argument]['default'], bool) and config[argument]['default'] != '':
            config[argument]['help'] += ' (default: %(default)s)'

        parser.add_argument('--' + argument, **config[argument])

    return parser

def cmdline():
    """
    Evalutate the command line.

    :rtype: argparse.Namespace
    :return: Command line arguments.
    """

    parser = cmdline_parser()
    args   = parser.parse_args()

    if args.config is None:
        parser.print_usage()
        exit(1)

    return args

class OniongrooveConfig:
    """
    Oniongroove class with configuration-related methods.
    """

    def get_config(self, item, default = None):
        """
        Helper to get instance configuration

        Retrieve a config parameter from the self.config object or use a
        default value as fallback

        :type  item: str
        :param item: Configuration item name

        :param default: Default config value to be used as a fallback if there's
                        no self.config[item] available.
                        Defaults to None

        :return: The configuration parameter value or the default fallback value.
        """

        if self.config is None:
            self.config = {}

        if item in self.config:
            return self.config[item]

        # Optionally override the default with an argument provided
        elif default is not None:
            self.config[item] = default

            return default

        return config[item]['default']
