#
# OpenResty HTTP rewriting proxy for Onion Services.
#
# This is part of the Oniongroove prototype:
# https://gitlab.torproject.org/tpo/onion-services/oniongroove/-/issues/3
#
# Adapted from    http://luajit.io/posts/asynchronous-lua-filter-for-nginx-response/
#                 https://github.com/openresty/lua-nginx-module/issues/1813#issuecomment-926589668
#                 https://gitlab.torproject.org/tpo/onion-services/onionspray/-/blob/main/templates/nginx.conf.txt
#
# With ideas from https://github.com/openresty/lua-nginx-module/issues/1813
#                 https://techoverflow.net/2021/09/11/how-to-replace-http-response-data-in-nginx-using-lua/
#
# Copyright (C) 2024 The Tor Project, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Always check upstream certificates
proxy_ssl_verify on;
proxy_ssl_trusted_certificate /etc/ssl/certs/ca-certificates.crt;

# Use SNI
proxy_ssl_server_name on;

# Enable TLS session reuse
proxy_ssl_session_reuse on;

# DNS for proxy
resolver 8.8.8.8 valid=15s;
resolver_timeout 15s;

# Lua initialization procedures
# https://openresty-reference.readthedocs.io/en/latest/Directives/#init_worker_by_lua_block
init_by_lua_file /etc/nginx/conf.d/init.lua;

# Handle header filtering in Lua
# https://openresty-reference.readthedocs.io/en/latest/Directives/#header_filter_by_lua_block
header_filter_by_lua_file /etc/nginx/conf.d/header.lua;

# Debug upstream
upstream debugger_upstream {
  server 127.0.0.1:8080;

  keepalive 320;
  keepalive_requests 10000;
  keepalive_timeout 60s;
}

# Debug upstream server
server {
  listen 0.0.0.0:8080 default_server reuseport;
  listen [::]:8080 default_server reuseport;

  location / {
    return 200 "Hello\n";
  }
}

server {
  listen 80;

  # Subdomain regexp captures trailing dot, use carefully; does not need "~*"
  server_name "~^(?<subdomains>([-0-9a-z]+\.)*)(?<onionaddr>[-0-9-a-z]{56}\.onion)$";

  location / {
    # Tell the client to try again as HTTPS without ever leaving the onion
    #
    # Use 307 / temporary redirect because your URIs may change in future.
    # Use $host (not $server) to copy-over subdomains, etc, transparently.
    # Send back original params, and fix them only upon forward to the proxy.
    return 307 https://$host$request_uri;
  }
}

# Adapted example from http://luajit.io/posts/asynchronous-lua-filter-for-nginx-response/
server {
  listen 443 ssl;

  # Subdomain regexp captures trailing dot, use carefully; does not need "~*"
  server_name "~^(?<subdomains>([-0-9a-z]+\.)*)(?<onionaddr>[-0-9-a-z]{56}\.onion)$";

  # Set the upstream domain
  set_by_lua $upstream_domain 'return reverse_mappings[ngx.arg[1]]' $onionaddr;

  # Deonionify the request_uri for forwarding (both path and args)
  set_by_lua_block $requested_uri { return ApplyReverseMap(ngx.var.request_uri) }

  # TLS configuration
  ssl_buffer_size 4k;
  # SSL_CERT_EACH_ONION is enabled, see individual onions for ssl_certificate directives
  #ssl_ciphers 'EECDH+CHACHA20:EECDH+AESGCM:EECDH+AES256'; ## LibreSSL, OpenSSL 1.1.0+
  ssl_ciphers 'EECDH+AESGCM:EECDH+AES256'; ## OpenSSL 1.0.1% to 1.0.2%
  ssl_ecdh_curve prime256v1;
  #ssl_ecdh_curve secp384r1:prime256v1; ## NGINX nginx 1.11.0 and later
  ssl_prefer_server_ciphers on;
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_session_cache shared:SSL:10m;
  ssl_session_timeout 10m;

  # Certificates
  #
  # From http://nginx.org/en/docs/http/ngx_http_ssl_module.html#ssl_certificate
  #
  # > Note that using variables implies that a certificate will be loaded for
  # > each SSL handshake, and this may have a negative impact on performance.
  #
  # This seems not to support all variables (as some are set in later contexts),
  # such as $onionaddr.
  #
  # Also, this does not play well with subdomains if using a wildcard cert.
  #
  # So this is only used as a fallback, and proper certificate and key are
  # set at`ssl_certificate_by_lua_block`.
  #ssl_certificate "/etc/nginx/certs.d/${onionaddr}.crt";
  #ssl_certificate_key "/etc/nginx/certs.d/${onionaddr}.pem";
  ssl_certificate "/etc/nginx/certs.d/${ssl_server_name}.crt";
  ssl_certificate_key "/etc/nginx/certs.d/${ssl_server_name}.pem";

  #Placeholders
  #ssl_certificate /path/to/fallback.crt;
  #ssl_certificate_key /path/to/fallback.key;

  # Lua TLS procedures
  #
  # This might have some (minor) negative performance impact as the
  # certificates are determined on the fly during handshake.
  #
  # Key and certificate files are generate in the first handshake, if they still do not exist.
  #
  # Adapted from https://github.com/openresty/lua-resty-core/blob/master/lib/ngx/ssl.md
  # Check also https://openresty-reference.readthedocs.io/en/latest/Directives/#ssl_certificate_by_lua_block
  #
  # In the future, this logic could be replaced by lua-resty-auto-ssl, assuming .onion certificates
  # are supported by Let's Encrypt: https://github.com/auto-ssl/lua-resty-auto-ssl
  ssl_certificate_by_lua_file /etc/nginx/conf.d/ssl.lua;

  # Internal proxy_pass location
  location /_onionproxy {
    internal;

    # Remove the subrequest URL prefix
    rewrite ^/_onionproxy/(.*)$ /$1 break;

    # Set the proxy version
    proxy_http_version 1.1;

    # Set the upstream host
    proxy_set_header Host "${subdomains}${upstream_domain}";

    # Rewrite/inject request origin
    set_by_lua_block $new_origin { return ApplyReverseMap(ngx.var.http_origin) }
    proxy_set_header Origin $new_origin;

    # Rewrite/inject request referer
    set_by_lua_block $new_referer { return ApplyReverseMap(ngx.var.http_referer) }
    proxy_set_header Referer $new_referer;

    # rewrite request cookies
    set_by_lua_block $new_cookie { return ApplyReverseMap(ngx.var.http_cookie) }
    proxy_set_header Cookie $new_cookie;

    # Tell upstream we prefer uncompressed data, since compressed responses
    # would not be properly processed by our filters.
    #
    # This workaround has performance impact.
    #
    # Ref. https://groups.google.com/g/openresty-en/c/Gj-s_hARc84/m/u_HqfSvMTk0J
    #proxy_set_header Accept-Encoding "";
    proxy_set_header Accept-Encoding "identity";

    # Tell upstream we're expecting the content offered through HTTPS
    proxy_set_header X-Forwarded-Proto https;

    # Connection handling
    proxy_set_header Connection "";

    # Do the upstream request, forcing HTTPS
    #proxy_pass https://example.org;
    #proxy_pass https://debugger_upstream;
    #proxy_pass "${scheme}://${subdomains}${upstream_domain}${requested_uri}";
    proxy_pass "https://${subdomains}${upstream_domain}${requested_uri}";
  }

  location / {
    # Quick debugging
    #return 200 "Subdomains: ${subdomains}, Onion address: ${onionaddr}\n";
    #return 200 "Upstream: https://${subdomains}${upstream_domain}${requested_uri}\n";
    #return 200 "Origin: $new_origin, Referer: $new_referer, Cookie: $new_cookie\n";

    # Content handling in Lua
    # https://openresty-reference.readthedocs.io/en/latest/Directives/#content_by_lua_block
    content_by_lua_file /etc/nginx/conf.d/content.lua;
  }
}
