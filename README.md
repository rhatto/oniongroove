# Oniongroove: Deployment tool for the Onion Web

![Oniongroove Logo](docs/assets/logo.jpg "Oniongroove")

Oniongroove is an upcoming suite for managing Onion Services:

* [Documentation](https://onionservices.torproject.org/apps/web/oniongroove)
  (also available at the [docs/](docs) folder).
* [Repository](https://gitlab.torproject.org/tpo/onion-services/oniongroove).
